<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="javax.servlet.Servlet,java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:genericpage>
	<jsp:attribute name="CSS"></jsp:attribute>
	<jsp:attribute name="JS"></jsp:attribute>
	<jsp:body>
		<div class="page-title">
		<div class="title-left">
			<h3>Picker Assignment</h3>
		</div>
		<div class="title_right">
            
        </div>
		</div>
		<div class="clearfix">
	    </div>

		<div class="row">
			<form action="${pageContext.request.contextPath}/AssignPicker" method="post">
				<div class="row">
					<div class="col-md-3">
						<select class="form-control" name="ddlPicker" id="listPicker">
							<c:forEach items="${listPicker}" var="picker">
								<option value="${picker}">${picker}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				
			
				<div class="row" style="margin-top: 20px;">
				<div class="x_panel">
					
					<div class="x_content">
					<table class="table">
						<thead>
							<tr>
								<th>Id Transaction</th><th>Username</th><th>Code Bank</th><th>Date</th><th>Address</th><th>Address Detail</th><th>Account</th><th>Nominal</th><th>Keterangan</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${listorder}" var="order">
							<tr>
								<td><c:out value="${order.idHistory}"/></td>
								<td><c:out value="${order.username}"/></td>
								<td><c:out value="${order.codeBank}"/></td>
								<td><c:out value="${order.date}"/></td>
								<td><c:out value="${order.address}"/></td>
								<td><c:out value="${order.addressDetail}"/></td>
								<td><c:out value="${order.account}"/></td>
								<td><c:out value="${order.nominal}"/></td>
								<td><c:out value="${order.keterangan}"/></td>
								<td><button type="submit" name="assign" value="${order.idHistory}" >Assign</button> </td>
							</tr>
						
						
						</c:forEach>
						</tbody>
					</table>
					</div>
					</div>
				</div>
			</form>
		
		</div>

	</jsp:body> 

</t:genericpage>