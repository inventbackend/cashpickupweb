<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="javax.servlet.Servlet,java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<t:genericpage>
	<jsp:attribute name="CSS"></jsp:attribute>
	<jsp:attribute name="JS">
	<script>
	$(document).ready(function() {
		$('#listStatus').on('change',function(){
			var ddl  = document.getElementById("listStatus");
			var strDdl = ddl.options[ddl.selectedIndex].value;
			$.ajax({
	            url : 'Dashboard',
	            data : {
 	                value : strDdl
	            },
	            success : function(responseXml) {
	            	/* $('#content').text(responseText); */
	            	$("#content").html($(responseXml).find("data").html());
	            	
	            	
	            }
	        });
		});
	});
	</script>
	</jsp:attribute>
	<jsp:body>
	<div class="page-title">
        <div class="title_left">
            <h3>Cash Pickup Dashboard</h3>
        </div>
        <div class="title_right">
            
        </div>
	    </div>
	    <div class="clearfix">
	    </div>

	    <div class="row">
	    	<div class="row">
	    		<div class="col-md-3">
						<select class="form-control" name="ddlStatus" id="listStatus">
							<option value="ONPROGRESS">On Progress</option>
							<option value="FINISH">Client Finish</option>
							<option value="SETTLE">Settlement</option>
						</select>
				</div>
	    	</div>
			<form action="${pageContext.request.contextPath}/Dashboard" method="post">
			<div class="x_panel" style="margin-top: 20px;">
				
			
				<div id="content"  class="x_content">
				<div class = "row">
					
				</div>
					
						<c:set var="conStatus1" scope="session" value = "${conStatus}"  />
						<c:choose>
							<c:when test="${empty conStatus1}">
							<data>
								<table class="table">
									<thead>
										<tr>
											<th>Id Transaction</th><th>Customer ID</th><th>Picker ID</th><th>Date</th><th>Address</th><th>Address Detail</th><th>Nominal</th><th>Keterangan</th><th>Status</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listorder}" var ="order">
										<tr>
											<td><c:out value="${order.idHistory}"/></td>
											<td><c:out value="${order.idClient}"/></td>
											<td><c:out value="${order.idPicker}"/></td>
											<td><c:out value="${order.date}"/></td>
											<td><c:out value="${order.address}"/></td>
											<td><c:out value="${order.addressDetail}"/></td>
											<td><c:out value="${order.nominal}"/></td>
											<td><c:out value="${order.keterangan}"/></td>
											<td><c:out value="${order.status}"/></td>
					
										</tr>
										
										</c:forEach>
									</tbody>
								</table>
							</data>
							</c:when>
							
							<c:when test="${conStatus1 == 'ONPROGRESS'}">
							<data>
								<table class="table">
									<thead>
										<tr>
											<th>Id Transaction</th><th>Customer ID</th><th>Picker ID</th><th>Date</th><th>Address</th><th>Address Detail</th><th>Nominal</th><th>Keterangan</th><th>Status</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listorder}" var ="order">
										<tr>
											<td><c:out value="${order.idHistory}"/></td>
											<td><c:out value="${order.idClient}"/></td>
											<td><c:out value="${order.idPicker}"/></td>
											<td><c:out value="${order.date}"/></td>
											<td><c:out value="${order.address}"/></td>
											<td><c:out value="${order.addressDetail}"/></td>
											<td><c:out value="${order.nominal}"/></td>
											<td><c:out value="${order.keterangan}"/></td>
											<td><c:out value="${order.status}"/></td>
					
										</tr>
										
										</c:forEach>
									</tbody>
								</table>
							</data>
							</c:when>
							
							<c:when test="${conStatus1 == 'FINISH'}">
								<data>
								<table class="table">
									<thead>
										<tr>
											<th>Id Transaction</th><th>Customer ID</th><th>Picker ID</th><th>Date</th><th>Address</th><th>Address Detail</th><th>Nominal</th><th>Actual Nominal</th><th>Keterangan</th><th>Status</th><th>Comment</th><th>Rating</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listorder}" var ="order">
										<tr>
											<td><c:out value="${order.idHistory}"/></td>
											<td><c:out value="${order.idClient}"/></td>
											<td><c:out value="${order.idPicker}"/></td>
											<td><c:out value="${order.date}"/></td>
											<td><c:out value="${order.address}"/></td>
											<td><c:out value="${order.addressDetail}"/></td>
											<td><c:out value="${order.nominal}"/></td>
											<td><c:out value="${order.actualNominal}"/></td>
											<td><c:out value="${order.keterangan}"/></td>
											<td><c:out value="${order.status}"/></td>
											<td><c:out value="${order.comment}"/></td>
											<td><c:out value="${order.rating}"/></td>
					
										</tr>
										
										</c:forEach>
									</tbody>
								</table>
							</data>
							</c:when>
							<c:otherwise>
								<data>
								<table class="table">
									<thead>
										<tr>
											<th>Id Transaction</th><th>Customer ID</th><th>Picker ID</th><th>Date</th><th>Address</th><th>Address Detail</th><th>Nominal</th><th>Actual Nominal</th><th>Status</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listorder}" var ="order">
										<tr>
											<td><c:out value="${order.idHistory}"/></td>
											<td><c:out value="${order.idClient}"/></td>
											<td><c:out value="${order.idPicker}"/></td>
											<td><c:out value="${order.date}"/></td>
											<td><c:out value="${order.address}"/></td>
											<td><c:out value="${order.addressDetail}"/></td>
											<td><c:out value="${order.nominal}"/></td>
											<td><c:out value="${order.actualNominal}"/></td>
											<td><c:out value="${order.status}"/></td>
					
										</tr>
										
										</c:forEach>
									</tbody>
								</table>
							</data>
							</c:otherwise>
						
						</c:choose>
						
					
			
				</div>
			</div>
			</form>
		</div>
	</jsp:body>
</t:genericpage>