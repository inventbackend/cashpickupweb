<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="javax.servlet.Servlet,java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:genericpage>
	<jsp:attribute name="CSS"></jsp:attribute>
	<jsp:attribute name="JS">
	<script>
	$(document).ready(function(){
		var rows = $('table.table tr');
		
		$(this).click(function(e){
			var identity = e.target.id;
			var className = $('#'+identity).attr('class');
			
			rows.filter('.'+identity).stop().toggle(100);
			$('#'+identity).removeClass('fa fa-chevron-down').addClass('fa fa-chevron-up');

			if(className == "fa fa-chevron-up"){
				$('#'+identity).removeClass('fa fa-chevron-up').addClass('fa fa-chevron-down');
			}
			
		});


	});
	</script>
	
	
	</jsp:attribute>
	<jsp:body>
	
		<div class="page-title">
        <div class="title_left">
            <h3>Order Acceptance</h3>
        </div>
        <div class="title_right">
            
        </div>
	    </div>
	    	<div class="clearfix">
	    </div>
		<div class="row">
			<form action="${pageContext.request.contextPath}/Order" method="post">
			<div class="x_panel">
				
			
				<div class="x_content">
				<table class="table">
				<thead>
					<tr>
						<th></th><th>Id Transaction</th><th>Username</th><th>Code Bank</th><th>Date</th><th>Account</th><th>Nominal</th>
					</tr>
				</thead>
				<tbody>
				
				<c:forEach items="${listorder}" var ="order">
				
				
				<tr>
					<td>
					<a><span id="${order.idHistory}" class="fa fa-chevron-down"></span></a></td>
					<td><c:out value="${order.idHistory}"/></td>
					<td><c:out value="${order.username}"/></td>
					<td><c:out value="${order.codeBank}"/></td>
					<td><c:out value="${order.date}"/></td>
					<td><c:out value="${order.account}"/></td>
					<td><c:out value="${order.nominal}"/></td>
					
					<td><button type="submit" name="select" value="${order.idHistory}" >Accept</button> </td>
				</tr>
				<tr class="${order.idHistory}" style="display:none;font-weight:bold;"><td></td><td>Address</td><td>Address Detail</td><td>Keterangan</td></tr>
				<tr class="${order.idHistory}" style="display:none;"><td></td><td><c:out value="${order.address}"/></td><td><c:out value="${order.addressDetail}"/></td><td><c:out value="${order.keterangan}"/></td></tr>
				</c:forEach>
				</tbody>
				</table>
				</div>
			</div>
			</form>
		</div>
	
	</jsp:body> 

</t:genericpage>