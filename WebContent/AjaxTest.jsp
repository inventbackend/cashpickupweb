<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="javax.servlet.Servlet,java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<t:genericpage>
	<jsp:attribute name="CSS"></jsp:attribute>
	<jsp:attribute name="JS">
	<script src="${pageContext.request.contextPath}/Scripts/jquery-1.11.3.js"></script>
	<script>
	$(document).ready(function() {
		$('#listPicker').on('change',function(){
			var ddl  = document.getElementById("listPicker");
			var strDdl = ddl.options[ddl.selectedIndex].value;
			$.ajax({
	            url : 'ajaxtest',
	            data : {
 	                value : strDdl
	            },
	            success : function(responseXml) {
	            	/* $('#content').text(responseText); */
	            	$("#content").html($(responseXml).find("data").html());
	            	
	            	
	            }
	        });
		});
	});
	</script>
	</jsp:attribute>
	<jsp:body>
		<div class="page-title">
		<div class="title-left">
			<h3>Picker Assignment</h3>
		</div>
		<div class="title_right">
            
        </div>
		</div>
		<div class="clearfix">
	    </div>

		<div class="row">
			<form action="${pageContext.request.contextPath}/AssignPicker" method="post">
				<div class="row">
					<div class="col-md-3">
						<select class="form-control" name="ddlPicker" id="listPicker">
							<c:forEach items="${listPicker}" var="picker">
								<option value="${picker}">${picker}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="row" style="margin-top: 20px;">
				<div class="x_panel">
					
					<div id="content" class="x_content">
					<data>
					<table class="table">
						<thead>
							<tr>
								<th>Id Transaction</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${listorder}" var="order">
							<tr>
								<td><c:out value="${order}"/></td>

								<td><button type="submit" name="assign" value="${order}" >Assign</button> </td>
							</tr>
						
						
						</c:forEach>
						</tbody>
					</table>
					</data>
					</div>
					</div>
				</div>
				
			</form>
		
		</div>
	</jsp:body>
</t:genericpage>