package database;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

import org.eclipse.jdt.internal.compiler.ast.ThrowStatement;

import com.sun.rowset.JdbcRowSetImpl;

public class RowSetAdapter {
	
//	public static JdbcRowSet getJDBCRowSet() throws SQLException{
//		
//		String databaseUrl = "jdbc:mysql://localhost:3306/cashpickup";
//	    String username = "client6";
//	    String password = "invent";
//		
//		RowSetFactory rsFactory = RowSetProvider.newFactory();
//		JdbcRowSet jRowSet = rsFactory.createJdbcRowSet();
//		
//		jRowSet.setUrl(databaseUrl);
//		jRowSet.setUsername(username);
//		jRowSet.setPassword(password);
//		jRowSet.setReadOnly(false);
//		
//		return jRowSet;
//	}
	
	
	private static Connection conn;
	public static JdbcRowSet getJDBCRowSet() throws Exception{
		InitialContext context = new InitialContext();
    	DataSource dataSource = (DataSource) context.lookup("java:/comp/env/jdbc/cashpickup");
    	conn = dataSource.getConnection();
    	JdbcRowSetImpl jdbcRs = new JdbcRowSetImpl(conn);

    	return jdbcRs;
	}
		 
	public static Connection getConnection()throws Exception{
		 
		 try {
		     InitialContext context = new InitialContext();
		     DataSource dataSource = (DataSource) context.lookup("java:/comp/env/jdbc/cashpickup");
			    		 
		     conn = dataSource.getConnection();
		      
		 } 
		 catch (Exception e) {
		      e.printStackTrace();
		 }
  
	     return conn;
	}

}
	

	


