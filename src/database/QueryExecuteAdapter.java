package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.sql.RowSet;

public class QueryExecuteAdapter {

	public static RowSet QueryExecute(String sql){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		RowSet rs = null;

		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement(sql);

			jrs = pstm.executeQuery();
			rs =(RowSet) jrs;

		}catch(Exception ex){
			System.out.println(ex);
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return rs;
	}

	public static RowSet QueryExecute(String sql, List<model.mdlQueryExecute> queryParam){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		RowSet rs = null;

		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement(sql);

			if (queryParam.size() >= 0){
				for (int i=0;i<=queryParam.size();i++){
					String type = queryParam.get(i).paramType;
					if (type.equalsIgnoreCase("string")){
						String value = (String) queryParam.get(i).paramValue;
						pstm.setString(i, value);
					}else if (type.equalsIgnoreCase("int")){
						int value = Integer.parseInt(queryParam.get(i).paramValue);
						pstm.setInt(i, value);
					}else if (type.equalsIgnoreCase("decimal") || type.equalsIgnoreCase("double") ){
						Double value = Double.parseDouble(queryParam.get(i).paramValue);
						pstm.setDouble(i, value);
					}else if (type.equalsIgnoreCase("boolean")){
						Boolean value = Boolean.parseBoolean(queryParam.get(i).paramValue);
						pstm.setBoolean(i, value);
					}else{
						String value = (String) queryParam.get(i).paramValue;
						pstm.setString(i, value);
					}
				}
			}

			jrs = pstm.executeQuery();
			rs =(RowSet) jrs;

		}catch(Exception ex){
			System.out.println(ex);
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return rs;
	}
}


