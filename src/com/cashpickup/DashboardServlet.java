package com.cashpickup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/Dashboard"})
public class DashboardServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2568052995837998395L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		List<model.mdlDashboardOnProgress> listTransactionOnprogress = new ArrayList<model.mdlDashboardOnProgress>();
		List<model.mdlDashboardClientFinish>listTransactionClientFinish = new ArrayList<model.mdlDashboardClientFinish>();
		List<model.mdlDashboardSettle> listTransactionSettle = new ArrayList<model.mdlDashboardSettle>();
		
		String action = request.getParameter("value");
//		String ddlStatus = request.getParameter("ddlStatus");
		request.setAttribute("conStatus", action); 
		
		if(action == null || "".equals(action)){
			listTransactionOnprogress.addAll(adapter.DashboardAdapter.GetTransactionOnProgressList());
			request.setAttribute("listorder", listTransactionOnprogress);
			RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard.jsp");
			dispacther.forward(request, response);
		}else if("ONPROGRESS".equals(action)){
			listTransactionOnprogress.addAll(adapter.DashboardAdapter.GetTransactionOnProgressList());
			request.setAttribute("listorder", listTransactionOnprogress);
			RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard.jsp");
			dispacther.forward(request, response);
		}else if("FINISH".equals(action)){
			listTransactionClientFinish.addAll(adapter.DashboardAdapter.GetTransactionClientFinish());
			request.setAttribute("listorder", listTransactionClientFinish);
			RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard.jsp");
			dispacther.forward(request, response);
		}else if("SETTLE".equals(action)){
			listTransactionSettle.addAll(adapter.DashboardAdapter.GetTransactionSettle());
			request.setAttribute("listorder", listTransactionSettle);
			RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard.jsp");
			dispacther.forward(request, response);
		}
		

		
	}
	
	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		

	}

}
