package com.cashpickup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javabean.exBean;

@WebServlet(urlPatterns = {"/Order"})
public class OrderListServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2691662606279964651L;
	

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		List<model.mdlOrderList> listOrderList = new ArrayList<model.mdlOrderList>();
		listOrderList.addAll(adapter.OrderListAdapter.GetOrderList());
		request.setAttribute("listorder", listOrderList);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/OrderList.jsp");
		dispacther.forward(request, response);
		
	}
	
	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		
		String idHistory = request.getParameter("select");
		
		if(idHistory !=null){
			adapter.OrderListAdapter.UpdateOrderStatus(idHistory);
		}
		
		doGet(request,response);
//		RequestDispatcher dispacther = request.getRequestDispatcher("/Order");
//		dispacther.forward(request, response);
//		response.sendRedirect("/Order");
	}
}
