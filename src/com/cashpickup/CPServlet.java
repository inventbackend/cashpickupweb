package com.cashpickup;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javabean.exBean;
import jdk.nashorn.internal.ir.RuntimeNode.Request;
import sun.rmi.server.Dispatcher;


@WebServlet(urlPatterns = {"/CP"})
public class CPServlet extends HttpServlet {
	
	
	
	private static final long serialVersionUID = -1220075802445250937L;
	
	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		exBean user = new exBean();
		user.setUsername(username);
		user.setPassword(password);
		
		
		
		HttpSession session  = request.getSession();
		session.setAttribute("sesUser", user );
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/EL.jsp");
		dispacther.forward(request, response);
	}

}
