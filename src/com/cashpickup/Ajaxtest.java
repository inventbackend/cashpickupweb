package com.cashpickup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/ajaxtest"})
public class Ajaxtest extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4730146334112098792L;
	
	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		List<String> listPicker = new ArrayList<String>();
		List<String> listOrderList = new ArrayList<String>();
		listPicker.addAll(adapter.AssignPickerAdapter.GetPicker());
		
		String ddlSelected = request.getParameter("value");
//		if(ddlSelected == ""){
//			listOrderList.addAll(adapter.AjaxTestAdapter.listIdTransaction(ddlSelected));
//		}
//		else{
//			listOrderList.addAll(adapter.AjaxTestAdapter.listIdTransaction(ddlSelected));
//		}
		

		request.setAttribute("listPicker", listPicker);
		
//		request.setAttribute("listorder", listOrderList);
		
		if(ddlSelected == null || "".equals(ddlSelected)){
			
			listOrderList.addAll(adapter.AjaxTestAdapter.listIdTransaction(ddlSelected));
			request.setAttribute("listorder", listOrderList);
			RequestDispatcher dispacther = request.getRequestDispatcher("/AjaxTest.jsp");
			dispacther.forward(request, response);
		}
		else{
			listOrderList.addAll(adapter.AjaxTestAdapter.listIdTransaction(ddlSelected));
			request.setAttribute("listorder", listOrderList);
			RequestDispatcher dispacther = request.getRequestDispatcher("/AjaxTest.jsp");
			dispacther.forward(request, response);
//			 response.setContentType("text/plain");
//		     response.getWriter().write(ddlSelected);
		}
		
	}
	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		
	}

}
