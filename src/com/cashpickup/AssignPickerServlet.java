package com.cashpickup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/AssignPicker"})
public class AssignPickerServlet extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3281670068464295058L;
	
	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		List<String> listPicker = new ArrayList<String>();
		List<model.mdlOrderList> listOrderList = new ArrayList<model.mdlOrderList>();
		
		listPicker.addAll(adapter.AssignPickerAdapter.GetPicker());
		listOrderList.addAll(adapter.AssignPickerAdapter.GetOnProcessList());
		request.setAttribute("listPicker", listPicker);
		request.setAttribute("listorder", listOrderList);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/AssignPicker.jsp");
		dispacther.forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		String idHistory = request.getParameter("assign");
		String idPicker = request.getParameter("ddlPicker");
		
		if(idHistory != null){
			adapter.AssignPickerAdapter.UpdateHistoryTransaction(idHistory);
			adapter.AssignPickerAdapter.InsertAssignPicker(idHistory, idPicker);
			adapter.AssignPickerAdapter.SendPushNotificationClient(idHistory);
			adapter.AssignPickerAdapter.SendPushNotificationPicker(idHistory, idPicker);
			
		}
		doGet(request,response);
		//response.sendRedirect("/AssignPicker");
	}
}
