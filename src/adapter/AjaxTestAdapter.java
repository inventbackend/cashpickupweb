package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

public class AjaxTestAdapter {
	public static List<String> listIdTransaction(String idPickerTemp){
		String idPicker = "";
		if(idPickerTemp == null || "".equals(idPickerTemp)){
			idPicker = "picker";
		}else{
			idPicker = idPickerTemp;
		}

		List<String> listOrder = new ArrayList<String>();

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;

		try{
//			Class.forName("com.mysql.jdbc.Driver");
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();
//			jrs.setCommand("select * from orderpicker WHERE IdPicker = ?");
//			jrs.setString(1, idPicker);
//			jrs.execute();

			connection = database.RowSetAdapter.getConnection();
			String sql = "select * from orderpicker WHERE IdPicker = ?";
			pstm = connection.prepareStatement(sql);
			pstm.setString(1, idPicker);
			jrs = pstm.executeQuery();

			while(jrs.next()){
				listOrder.add(jrs.getString("IdHistory"));
			}

		}catch(Exception ex){
			System.out.println(ex);
		}finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}

		return listOrder;
	}

}
