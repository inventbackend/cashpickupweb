package adapter;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import org.apache.jasper.tagplugins.jstl.core.Catch;

import com.sun.rowset.JdbcRowSetImpl;

public class AssignPickerAdapter {

	public static List<model.mdlOrderList> GetOnProcessList(){
		List<model.mdlOrderList> listmdlOrderList = new ArrayList<model.mdlOrderList>();

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;

		try{
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();
//			jrs.setCommand("SELECT * FROM historytransaction WHERE Status = 'ONPROCESS'");
//			jrs.execute();

			connection = database.RowSetAdapter.getConnection();
			String sql = "SELECT * FROM historytransaction WHERE Status = 'ONPROCESS'";
			pstm = connection.prepareStatement(sql);

			jrs = pstm.executeQuery();

			while(jrs.next()){
				model.mdlOrderList mdlOrderList = new model.mdlOrderList();
				mdlOrderList.setIdHistory(jrs.getString("IdHistory"));
				mdlOrderList.setUsername(jrs.getString("Username"));
				mdlOrderList.setCodeBank(jrs.getString("CodeBank"));
				mdlOrderList.setDate(jrs.getString("Date"));
				mdlOrderList.setAddress(jrs.getString("Address"));
				mdlOrderList.setAddressDetail(jrs.getString("AddressDetail"));
				mdlOrderList.setAccount(jrs.getString("Account"));
				mdlOrderList.setNominal(jrs.getString("Nominal"));
				mdlOrderList.setKeterangan(jrs.getString("Keterangan"));
				listmdlOrderList.add(mdlOrderList);
			}

		}catch(Exception ex){
			System.out.println(ex);
		}finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}


		return listmdlOrderList;
	}

	public static model.mdlOrderList GetOrderByIdHistory(String idHistory){
		model.mdlOrderList Order = new model.mdlOrderList();

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;

		try{
//			Class.forName("com.mysql.jdbc.Driver");
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();
//			jrs.setCommand("SELECT * FROM historytransaction WHERE IdHistory = ? ");
//			jrs.setString(1, idHistory);
//			jrs.execute();

			connection = database.RowSetAdapter.getConnection();
			String sql = "SELECT * FROM historytransaction WHERE IdHistory = ? ";
			pstm = connection.prepareStatement(sql);
			pstm.setString(1, idHistory);
			jrs = pstm.executeQuery();

			while(jrs.next()){
				Order.setIdHistory(jrs.getString("IdHistory"));
				Order.setUsername(jrs.getString("Username"));
				Order.setCodeBank(jrs.getString("CodeBank"));
				Order.setDate(jrs.getString("Date"));
				Order.setAddress(jrs.getString("Address"));
				Order.setAddressDetail(jrs.getString("AddressDetail"));
				Order.setLatitude(jrs.getString("Latitude"));
				Order.setLongitude(jrs.getString("Longitude"));
				Order.setAccount(jrs.getString("Account"));
				Order.setNominal(jrs.getString("Nominal"));
				Order.setKeterangan(jrs.getString("Keterangan"));
				Order.setStatus(jrs.getString("Status"));
			}

		}catch(Exception ex){
			System.out.println(ex);
		}finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}

		return Order;

	}

	public static model.mdlOrderDetail GetOrderDataToPicker(String idHistory){
		model.mdlOrderDetail orderData = new model.mdlOrderDetail();

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;

		try{
//			Class.forName("com.mysql.jdbc.Driver");
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();
//			jrs.setCommand("SELECT a.IdHistory,a.Username,e.CustomerName,a.CodeBank,a.Address,a.AddressDetail,d.Phone,a.Date,a.Latitude,a.Longitude,a.Account,a.Nominal,a.Keterangan,a.`Status`,c.Token FROM historytransaction a INNER JOIN orderpicker b ON a.IdHistory = b.IdHistory INNER JOIN pickeruser c ON b.IdPicker = c.Username INNER JOIN employee d ON c.Username = d.Username INNER JOIN customer e ON a.Username = e.Username WHERE a.IdHistory = ? ");
//			jrs.setString(1, idHistory);
//			jrs.execute();

			connection = database.RowSetAdapter.getConnection();
			String sql = "SELECT a.IdHistory,a.Username,e.CustomerName,a.CodeBank,a.Address,a.AddressDetail,d.Phone,a.Date,a.Latitude,"
						+ "a.Longitude,a.Account,a.Nominal,a.Keterangan,a.`Status`,c.Token FROM historytransaction a "
						+ "INNER JOIN orderpicker b ON a.IdHistory = b.IdHistory "
						+ "INNER JOIN pickeruser c ON b.IdPicker = c.Username "
						+ "INNER JOIN employee d ON c.Username = d.Username "
						+ "INNER JOIN customer e ON a.Username = e.Username WHERE a.IdHistory = ? ";
			pstm = connection.prepareStatement(sql);
			pstm.setString(1, idHistory);
			jrs = pstm.executeQuery();

			while(jrs.next()){
				orderData.setIdTransaction(jrs.getString("IdHistory"));
				orderData.setIdCustomer(jrs.getString("Username"));
				orderData.setCustomerName(jrs.getString("CustomerName"));
				orderData.setCodeBank(jrs.getString("CodeBank"));
				orderData.setAddress(jrs.getString("Address"));
				orderData.setAddressDetail(jrs.getString("AddressDetail"));
				orderData.setPhone(jrs.getString("Phone"));
				orderData.setDate(jrs.getString("Date"));
				orderData.setLatitude(jrs.getString("Latitude"));
				orderData.setLongitude(jrs.getString("Longitude"));
				orderData.setAccountNo(jrs.getString("Account"));
				orderData.setNominal(jrs.getString("Nominal"));
				orderData.setDescription(jrs.getString("Keterangan"));
				orderData.setStatus(jrs.getString("Status"));
				orderData.setToken(jrs.getString("Token"));
			}

		}catch(Exception ex){
			System.out.println(ex);
		}finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}

		return orderData;
	}

	public static model.mdlPickerDetail GetPickerDetailToClient(String idHistory){
		model.mdlPickerDetail clientData = new model.mdlPickerDetail();

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;

		try{
//			Class.forName("com.mysql.jdbc.Driver");
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();
//			jrs.setCommand("Select a.IdHistory,a.Date,a.Username as IdCustomer,b.Token,d.Username,d.FullName,d.Phone,d.Image,a.Status FROM historytransaction a INNER JOIN `user` b ON a.Username = b.Username INNER JOIN orderpicker c ON a.IdHistory = c.IdHistory INNER JOIN employee d ON c.IdPicker = d.Username WHERE a.IdHistory = ?");
//			jrs.setString(1, idHistory);
//			jrs.execute();

			connection = database.RowSetAdapter.getConnection();
			String sql = "Select a.IdHistory,a.Date,a.Username as IdCustomer,b.Token,d.Username,d.FullName,d.Phone,d.Image,a.Status "
						+ "FROM historytransaction a "
						+ "INNER JOIN `user` b ON a.Username = b.Username "
						+ "INNER JOIN orderpicker c ON a.IdHistory = c.IdHistory "
						+ "INNER JOIN employee d ON c.IdPicker = d.Username WHERE a.IdHistory = ?";
			pstm = connection.prepareStatement(sql);
			pstm.setString(1, idHistory);
			jrs = pstm.executeQuery();

			while(jrs.next()){
				clientData.setIdTransaction(jrs.getString("IdHistory"));
				clientData.setIdPicker(jrs.getString("Username"));
				clientData.setIdCustomer(jrs.getString("IdCustomer"));
				clientData.setDate(jrs.getString("Date"));
				clientData.setToken(jrs.getString("Token"));
				clientData.setIdPicker(jrs.getString("Username"));
				clientData.setNamePicker(jrs.getString("FullName"));
				clientData.setPhone(jrs.getString("Phone"));
//				clientData.setImage(jrs.getString("Image"));
				clientData.setStatus(jrs.getString("Status"));
			}

		}catch(Exception ex){
			System.out.println(ex);
		}finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}

		return clientData;
	}

	public static String GetIdCustomerByIdHistory(String idHistory){

		String idCustomer = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;

		try{
//			Class.forName("com.mysql.jdbc.Driver");
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();
//			jrs.setCommand("Select Username FROM historytransaction WHERE IdHistory = ?");
//			jrs.setString(1, idHistory);
//			jrs.execute();

			connection = database.RowSetAdapter.getConnection();
			String sql = "Select Username FROM historytransaction WHERE IdHistory = ?";
			pstm = connection.prepareStatement(sql);
			pstm.setString(1, idHistory);
			jrs = pstm.executeQuery();

			while(jrs.next()){
				idCustomer = jrs.getString("Username");
			}

		}catch(Exception ex){
			System.out.println(ex);
		}finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}

		return idCustomer;
	}

	public static void UpdateHistoryTransaction(String idHistory){

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;

		try{
//			Class.forName("com.mysql.jdbc.Driver");
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();
//			jrs.setCommand("UPDATE historytransaction SET Status = 'ACCEPTED' WHERE IdHistory = ? ");
//			jrs.setString(1, idHistory);
//			jrs.execute();

			connection = database.RowSetAdapter.getConnection();
			String sql = "UPDATE historytransaction SET Status = 'ACCEPTED' WHERE IdHistory = ? ";
			pstm = connection.prepareStatement(sql);
			pstm.setString(1, idHistory);
			pstm.execute();

		}catch(Exception ex){
			System.out.println(ex);
		}finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void InsertAssignPicker(String idHistory,String idPicker){

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;

		try{
//			Class.forName("com.mysql.jdbc.Driver");
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();
//			jrs.setCommand("SELECT * FROM orderpicker LIMIT 1");
//			jrs.execute();
//			jrs.moveToInsertRow();
//			jrs.updateString("IdPicker", idPicker);
//			jrs.updateString("IdHistory", idHistory);
//			jrs.insertRow();
//			jrs.close();

			connection = database.RowSetAdapter.getConnection();
			String sql = "INSERT INTO orderpicker (IdPicker,IdHistory) VALUES (?,?)";
			pstm = connection.prepareStatement(sql);
			pstm.setString(1, idPicker);
			pstm.setString(2, idHistory);
			pstm.execute();

		}catch(Exception ex){
			System.out.println(ex);
		}finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static List<String> GetPicker(){

		List<String> listPicker = new ArrayList<String>();

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;

		try{
//			Class.forName("com.mysql.jdbc.Driver");
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();
//			jrs.setCommand("select * from pickeruser");
//			jrs.execute();

			connection = database.RowSetAdapter.getConnection();
			String sql = "select * from pickeruser";
			pstm = connection.prepareStatement(sql);
			jrs = pstm.executeQuery();

			while(jrs.next()){
				listPicker.add(jrs.getString("Username"));
			}

		}catch(Exception ex){
			System.out.println(ex);
		}finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}

		return listPicker;
	}

	public static void SendPushNotificationClient(String idHistory){

		String message = "Your Cash Picker Is Ready.";
		String title = "CPIMAGE";

		List<String> listRegId = new ArrayList<String>();

		model.mdlPickerDetail pickerDetail = new model.mdlPickerDetail();
		pickerDetail = GetPickerDetailToClient(idHistory);

		listRegId.add(pickerDetail.getToken());

		model.mdlDataClient data = new model.mdlDataClient();

		data.setTitle(title);
		data.setIdTransaction(pickerDetail.getIdTransaction());
		data.setIdPicker(pickerDetail.getIdPicker());
		data.setNamePicker(pickerDetail.getNamePicker());
		data.setPhone(pickerDetail.getPhone());
//		data.setImage(pickerDetail.getImage());
		data.setStatus(pickerDetail.getStatus());
//		data.setBarcode(pickerDetail.getIdTransaction() + pickerDetail.getIdCustomer() + adapter.VariableAdapter.ChangeDateFormat("yyyy-M-dd hh:mm:ss", "yyyyMddhhmmss", pickerDetail.getDate()));

		adapter.PushNotificationAdapter.SendNotificationClient(message, title, listRegId, data);

		String idCustomer = GetIdCustomerByIdHistory(idHistory);
		adapter.NotificationConfirmationAdapter.InsertClientNotificationLog(idHistory, idCustomer, title);
	}

	public static void SendPushNotificationPicker(String idHistory, String idPicker){
		String message = "You Got One New Order.";
		String title = "CPACCEPTED";

		List<String> listRegId = new ArrayList<String>();

		model.mdlOrderDetail orderDetail = new model.mdlOrderDetail();
		orderDetail = GetOrderDataToPicker(idHistory);


		listRegId.add(orderDetail.getToken());


		model.mdlDataPicker data = new model.mdlDataPicker();

		data.setTitle(title);
		data.setIdTransaction(orderDetail.getIdTransaction());
		data.setIdCustomer(orderDetail.getIdCustomer());
		data.setCustomerName(orderDetail.getCustomerName());
		data.setCodeBank(orderDetail.getCodeBank());
		data.setAddress(orderDetail.getCodeBank());
		data.setAddressDetail(orderDetail.getAddressDetail());
		data.setPhone(orderDetail.getPhone());
		data.setDate(orderDetail.getDate());
		data.setLatitude(orderDetail.getLatitude());
		data.setLongitude(orderDetail.getLongitude());
		data.setAccountNo(orderDetail.getAccountNo());
		data.setNominal(orderDetail.getNominal());
		data.setDescription(orderDetail.getDescription());
		data.setStatus(orderDetail.getStatus());
//		data.setBarcode(orderDetail.getIdTransaction() + orderDetail.getIdCustomer() + adapter.VariableAdapter.ChangeDateFormat("yyyy-M-dd hh:mm:ss", "yyyyMddhhmmss", orderDetail.getDate()));

		adapter.PushNotificationAdapter.SendNotificationPicker(message, title, listRegId, data);
		adapter.NotificationConfirmationAdapter.InsertPickerNotificationLog(idHistory,idPicker,title);
	}

}


