package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

public class OrderListAdapter {

	public static List<model.mdlOrderList> GetOrderList(){
		List<model.mdlOrderList> listmdlOrderList = new ArrayList<model.mdlOrderList>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;

		try{
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();
//			jrs.setCommand("SELECT * FROM historytransaction WHERE Status = 'ORDER'");
//			jrs.execute();
			//jrs.close();

			connection = database.RowSetAdapter.getConnection();
			String sql = "SELECT * FROM historytransaction WHERE Status = 'ORDER'";
			pstm = connection.prepareStatement(sql);
			jrs = pstm.executeQuery();

			while(jrs.next()){
				model.mdlOrderList mdlOrderList = new model.mdlOrderList();
				mdlOrderList.setIdHistory(jrs.getString("IdHistory"));
				mdlOrderList.setUsername(jrs.getString("Username"));
				mdlOrderList.setCodeBank(jrs.getString("CodeBank"));
				mdlOrderList.setDate(jrs.getString("Date"));
				mdlOrderList.setAddress(jrs.getString("Address"));
				mdlOrderList.setAddressDetail(jrs.getString("AddressDetail"));
				mdlOrderList.setAccount(jrs.getString("Account"));
				mdlOrderList.setNominal(jrs.getString("Nominal"));
				mdlOrderList.setKeterangan(jrs.getString("Keterangan"));
				listmdlOrderList.add(mdlOrderList);
			}

		}catch(Exception ex){
			System.out.println(ex);
		}finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}

		return listmdlOrderList;
	}


	public static void UpdateOrderStatus(String idHistory) {

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		try{
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();
//			jrs.setCommand("SELECT * FROM historytransaction WHERE IdHistory = ? ");
//			jrs.setString(1, idHistory);
//			jrs.execute();
//			jrs.last();
//			int rowNum = jrs.getRow();
//			jrs.absolute(rowNum);
//			jrs.updateString("Status", "ONPROCESS");
//			jrs.updateRow();
//			jrs.close();

			connection = database.RowSetAdapter.getConnection();
			String sql = "UPDATE historytransaction SET Status = 'ONPROCESS' WHERE IdHistory = ? ";
			pstm = connection.prepareStatement(sql);
			pstm.setString(1, idHistory);
			pstm.execute();

		}catch(Exception ex){
			System.out.println(ex);
		}finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
}
