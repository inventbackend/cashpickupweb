package adapter;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

public class PushNotificationAdapter {
	
	public static void SendNotificationClient(String message,String title,List<String> listRegId,model.mdlDataClient data){
		System.out.println( "Sending POST to GCM" );

        String apiKey = "AIzaSyDRphEly0JYuhRRqjZ-49eiX6yT5cvoYDM";
        //Content content = createContent();
        
        model.mdlFCMClient content = new model.mdlFCMClient();
        model.mdlNotification notif = new model.mdlNotification();
        notif.setText(message);
        notif.setTitle(title);
        notif.setClick_action("ACTIVITY_ACCEPTED");
        
        
        List<String> regid = new ArrayList<String>();
        
       
        
        for(String temp : listRegId){
        	regid.add(temp);
        }
        	
        
        content.setCollapse_key("CashPickupClient");
        content.setDelay_while_idle(true);
        content.setTime_to_live(360);
        content.setPriority("high");
        content.setNotification(notif);
        content.setData(data);
        content.setRegistration_ids(regid);
        
        adapter.PushNotificationAdapter.PostToGCMClient(apiKey, content);
	}
	
	public static void SendNotificationPicker(String message,String title,List<String> listRegId,model.mdlDataPicker data){
		System.out.println( "Sending POST to GCM" );

        String apiKey = "AIzaSyAFVz63KrHX24S-ZW3H30tspxvXarPJSNc";
        //Content content = createContent();
        
        model.mdlFCMPicker content = new model.mdlFCMPicker();
        model.mdlNotification notif = new model.mdlNotification();
        notif.setText(message);
        notif.setTitle(title);
        notif.setClick_action("ACTIVITY_ACCEPTED");

        
        List<String> regid = new ArrayList<String>();
        
       
        
        for(String temp : listRegId){
        	regid.add(temp);
        }
        	
        content.setCollapse_key("CashPickupDemo");
        content.setDelay_while_idle(true);
        content.setPriority("high");
        content.setNotification(notif);
        content.setData(data);
        content.setTime_to_live(360);
        content.setRegistration_ids(regid);
        
        adapter.PushNotificationAdapter.PostToGCMPicker(apiKey, content);
	}
	
	public static void PostToGCMClient(String apiKey, model.mdlFCMClient content){
		try{

	        // 1. URL
	        URL url = new URL("https://fcm.googleapis.com/fcm/send");

	        // 2. Open connection
	        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

	        // 3. Specify POST method
	        conn.setRequestMethod("POST");

	        // 4. Set the headers
	        conn.setRequestProperty("Content-Type", "application/json");
	        conn.setRequestProperty("Authorization", "key="+apiKey);

	        conn.setDoOutput(true);
	        
	        Gson gson = new Gson();
	        
	        String json = gson.toJson(content);
	        
	        System.out.println(json);
	        

	            // 5. Add JSON data into POST request body

	            //`5.1 Use Jackson object mapper to convert Contnet object into JSON

	            // 5.2 Get connection output stream
	            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());

	            // 5.3 Copy Content "JSON" into
	            //mapper.writeValue(wr, content);
	            wr.write(json.getBytes("UTF-8"));
	            

	            // 5.4 Send the request
	            wr.flush();

	            // 5.5 close
	            wr.close();

	            // 6. Get the response
	            int responseCode = conn.getResponseCode();
	            System.out.println("\nSending 'POST' request to URL : " + url);
	            System.out.println("Response Code : " + responseCode);

	            BufferedReader in = new BufferedReader(
	                    new InputStreamReader(conn.getInputStream()));
	            String inputLine;
	            StringBuffer response = new StringBuffer();

	            while ((inputLine = in.readLine()) != null) {
	                response.append(inputLine);
	            }
	            in.close();

	            // 7. Print result
	            System.out.println(response.toString());

	            } catch (MalformedURLException e) {
	                e.printStackTrace();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
		
	}


	public static void PostToGCMPicker(String apiKey, model.mdlFCMPicker content){
		try{

	        // 1. URL
	        URL url = new URL("https://fcm.googleapis.com/fcm/send");

	        // 2. Open connection
	        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

	        // 3. Specify POST method
	        conn.setRequestMethod("POST");

	        // 4. Set the headers
	        conn.setRequestProperty("Content-Type", "application/json");
	        conn.setRequestProperty("Authorization", "key="+apiKey);

	        conn.setDoOutput(true);
	        
	        Gson gson = new Gson();
	        
	        String json = gson.toJson(content);
	        
	        System.out.println(json);
	        

	            // 5. Add JSON data into POST request body

	            //`5.1 Use Jackson object mapper to convert Contnet object into JSON

	            // 5.2 Get connection output stream
	            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());

	            // 5.3 Copy Content "JSON" into
	            //mapper.writeValue(wr, content);
	            wr.write(json.getBytes("UTF-8"));
	            

	            // 5.4 Send the request
	            wr.flush();

	            // 5.5 close
	            wr.close();

	            // 6. Get the response
	            int responseCode = conn.getResponseCode();
	            System.out.println("\nSending 'POST' request to URL : " + url);
	            System.out.println("Response Code : " + responseCode);

	            BufferedReader in = new BufferedReader(
	                    new InputStreamReader(conn.getInputStream()));
	            String inputLine;
	            StringBuffer response = new StringBuffer();

	            while ((inputLine = in.readLine()) != null) {
	                response.append(inputLine);
	            }
	            in.close();

	            // 7. Print result
	            System.out.println(response.toString());

	            } catch (MalformedURLException e) {
	                e.printStackTrace();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
		
	}
}
