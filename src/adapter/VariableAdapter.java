package adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class VariableAdapter {
	
	public static String ChangeDateFormat(String oldFormat,String newFormat,String oldDate){
		
		String newDate;
		SimpleDateFormat sdf = new SimpleDateFormat(oldFormat);
		Date tempDate = new Date();
		try {
			tempDate = sdf.parse(oldDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern(newFormat);
		newDate = sdf.format(tempDate);
		
		return newDate;
		
	}

}
