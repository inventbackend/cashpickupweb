package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

public class DashboardAdapter {
	
	public static List<model.mdlDashboardOnProgress> GetTransactionOnProgressList(){
		
		List<model.mdlDashboardOnProgress> listOnProgress = new ArrayList<model.mdlDashboardOnProgress>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		
		try{
			
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();	
			connection = database.RowSetAdapter.getConnection();
			String sql = "SELECT * FROM historytransaction a INNER JOIN orderpicker b ON a.IdHistory = b.IdHistory WHERE a.Status = 'ONPROCESS' OR a.Status = 'ACCEPTED' OR a.Status = 'VERIFIED'";	
			pstm = connection.prepareStatement(sql);
			
			jrs = pstm.executeQuery();

			while(jrs.next()){
				model.mdlDashboardOnProgress mdlOrderList = new model.mdlDashboardOnProgress();
				mdlOrderList.setIdHistory(jrs.getString("IdHistory"));
				mdlOrderList.setIdClient(jrs.getString("Username"));
				mdlOrderList.setIdPicker(jrs.getString("IdPicker"));
				mdlOrderList.setDate(jrs.getString("Date"));
				mdlOrderList.setAddress(jrs.getString("Address"));
				mdlOrderList.setAddressDetail(jrs.getString("AddressDetail"));
				mdlOrderList.setNominal(jrs.getString("Nominal"));
				mdlOrderList.setKeterangan(jrs.getString("Keterangan"));
				mdlOrderList.setStatus(jrs.getString("Status"));
				listOnProgress.add(mdlOrderList);
				
			}		
		}catch(Exception ex){
			System.out.println(ex);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				//LogAdapter.InsertLogExc(e.toString(), "LoadBank", "close opened connection protocol", Globals.user);
			}
		}
		
		return listOnProgress;
		
	}
	
	public static List<model.mdlDashboardClientFinish> GetTransactionClientFinish(){
		List<model.mdlDashboardClientFinish> listClientFinish = new ArrayList<model.mdlDashboardClientFinish>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
			
		try{
			connection = database.RowSetAdapter.getConnection();
			String sql = "SELECT * FROM historytransaction a INNER JOIN orderpicker b ON a.IdHistory = b.IdHistory WHERE a.Status = 'FINISH'";	
			pstm = connection.prepareStatement(sql);
			jrs = pstm.executeQuery();
			
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();	
//			jrs.setCommand("SELECT * FROM historytransaction a INNER JOIN orderpicker b ON a.IdHistory = b.IdHistory WHERE a.Status = 'FINISH'");		
//			jrs.execute();

			
			while(jrs.next()){
				model.mdlDashboardClientFinish mdlOrderList = new model.mdlDashboardClientFinish();
				mdlOrderList.setIdHistory(jrs.getString("IdHistory"));
				mdlOrderList.setIdClient(jrs.getString("Username"));
				mdlOrderList.setIdPicker(jrs.getString("IdPicker"));
				mdlOrderList.setDate(jrs.getString("Date"));
				mdlOrderList.setAddress(jrs.getString("Address"));
				mdlOrderList.setAddressDetail(jrs.getString("AddressDetail"));
				mdlOrderList.setNominal(jrs.getString("Nominal"));
				mdlOrderList.setActualNominal(jrs.getString("ActualNominal"));
				mdlOrderList.setKeterangan(jrs.getString("Keterangan"));
				mdlOrderList.setStatus(jrs.getString("Status"));
				mdlOrderList.setComment(jrs.getString("Comment"));
				mdlOrderList.setRating(jrs.getString("Rating"));
				listClientFinish.add(mdlOrderList);
				
			}
					
			//jrs.close();
		}catch(Exception ex){
			System.out.println(ex);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				//LogAdapter.InsertLogExc(e.toString(), "LoadBank", "close opened connection protocol", Globals.user);
			}
		}
		
		return listClientFinish;
	}
	
	public static List<model.mdlDashboardSettle> GetTransactionSettle(){
		List<model.mdlDashboardSettle> listSettle = new ArrayList<model.mdlDashboardSettle>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			String sql = "SELECT * FROM historytransaction a INNER JOIN orderpicker b ON a.IdHistory = b.IdHistory WHERE a.Status = 'SETTLE'";	
			pstm = connection.prepareStatement(sql);		
			jrs = pstm.executeQuery();
			
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();		
//			jrs.setCommand("SELECT * FROM historytransaction a INNER JOIN orderpicker b ON a.IdHistory = b.IdHistory WHERE a.Status = 'SETTLE'");		
//			jrs.execute();
				
			while(jrs.next()){
				model.mdlDashboardSettle mdlOrderList = new model.mdlDashboardSettle();
				mdlOrderList.setIdHistory(jrs.getString("IdHistory"));
				mdlOrderList.setIdClient(jrs.getString("Username"));
				mdlOrderList.setIdPicker(jrs.getString("IdPicker"));
				mdlOrderList.setDate(jrs.getString("Date"));
				mdlOrderList.setAddress(jrs.getString("Address"));
				mdlOrderList.setAddressDetail(jrs.getString("AddressDetail"));
				mdlOrderList.setNominal(jrs.getString("Nominal"));
				mdlOrderList.setActualNominal(jrs.getString("ActualNominal"));
				mdlOrderList.setStatus(jrs.getString("Status"));

				listSettle.add(mdlOrderList);
				
			}
			//jrs.close();
		}catch(Exception ex){
			System.out.println(ex);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				//LogAdapter.InsertLogExc(e.toString(), "LoadBank", "close opened connection protocol", Globals.user);
			}
		}
		
		return listSettle;
	}

}
