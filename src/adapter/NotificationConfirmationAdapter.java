package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

public class NotificationConfirmationAdapter {

	public static void InsertPickerNotificationLog(String idHistory, String idPicker,String title){

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;

		try{
//			Class.forName("com.mysql.jdbc.Driver");
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();
//			jrs.setCommand("SELECT * FROM notificationlog LIMIT 1");
//			jrs.execute();
//			jrs.moveToInsertRow();
//			jrs.updateString("IdHistory", idHistory);
//			jrs.updateString("Title", title);
//			jrs.updateInt("Sent", 1);
//			jrs.updateInt("Recieved", 0);
//			jrs.updateString("IdPicker", idPicker);
//			jrs.insertRow();
//			jrs.close();

			connection = database.RowSetAdapter.getConnection();
			String sql = "INSERT INTO notificationlog (IdHistory, Title, Sent, Recieved, IdPicker) VALUES (?, ?, ?, ?, ?)";
			pstm = connection.prepareStatement(sql);
			pstm.setString(1, idHistory);
			pstm.setString(2, title);
			pstm.setInt(3, 1);
			pstm.setInt(4, 0);
			pstm.setString(5, idPicker);
			pstm.execute();

		}catch(Exception ex){
			System.out.println(ex);
		}finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void InsertClientNotificationLog(String idHistory, String Username,String title){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;

		try{
//			Class.forName("com.mysql.jdbc.Driver");
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();
//			jrs.setCommand("SELECT * FROM notificationlogclient LIMIT 1");
//			jrs.execute();
//			jrs.moveToInsertRow();
//			jrs.updateString("IdHistory", idHistory);
//			jrs.updateString("Title", title);
//			jrs.updateInt("Sent", 1);
//			jrs.updateInt("Recieved", 0);
//			jrs.updateString("Username", Username);
//			jrs.insertRow();
//			jrs.close();

			connection = database.RowSetAdapter.getConnection();
			String sql = "INSERT INTO notificationlogclient (IdHistory, Title, Sent, Recieved, Username) VALUES (?, ?, ?, ?, ?)";
			pstm = connection.prepareStatement(sql);
			pstm.setString(1, idHistory);
			pstm.setString(2, title);
			pstm.setInt(3, 1);
			pstm.setInt(4, 0);
			pstm.setString(5, Username);
			pstm.execute();

		}catch(Exception ex){
			System.out.println(ex);
		}finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}


}
