package model;

public class mdlDashboardOnProgress {
	
	public String IdHistory;
	public String IdClient;
	public String IdPicker;
	public String Date;
	public String Address;
	public String AddressDetail;
	public String Nominal;
	public String Keterangan;
	public String Status;
	public String getIdHistory() {
		return IdHistory;
	}
	public void setIdHistory(String idHistory) {
		IdHistory = idHistory;
	}
	public String getIdClient() {
		return IdClient;
	}
	public void setIdClient(String idClient) {
		IdClient = idClient;
	}
	public String getIdPicker() {
		return IdPicker;
	}
	public void setIdPicker(String idPicker) {
		IdPicker = idPicker;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getAddressDetail() {
		return AddressDetail;
	}
	public void setAddressDetail(String addressDetail) {
		AddressDetail = addressDetail;
	}
	public String getNominal() {
		return Nominal;
	}
	public void setNominal(String nominal) {
		Nominal = nominal;
	}
	public String getKeterangan() {
		return Keterangan;
	}
	public void setKeterangan(String keterangan) {
		Keterangan = keterangan;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	
	
	
}
