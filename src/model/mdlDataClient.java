package model;

public class mdlDataClient {
	
	public String title;
	public String IdPicker;
	public String IdTransaction;
	public String NamePicker;
	public String Phone;
	public String Status;

	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getIdPicker() {
		return IdPicker;
	}
	public void setIdPicker(String idPicker) {
		IdPicker = idPicker;
	}
	public String getIdTransaction() {
		return IdTransaction;
	}
	public void setIdTransaction(String idTransaction) {
		IdTransaction = idTransaction;
	}
	public String getNamePicker() {
		return NamePicker;
	}
	public void setNamePicker(String namePicker) {
		NamePicker = namePicker;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}

	
	
	

}
