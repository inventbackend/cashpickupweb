package model;

import java.util.Date;

public class mdlOrderList {
	public String IdHistory;
	public String Username;
	public String CodeBank;
	public String Date;
	public String Address;
	public String AddressDetail;
	public String Latitude;
	public String Longitude;
	public String Account;
	public String Nominal;
	public String Keterangan;
	public String Status;
	
	public String getIdHistory() {
		return IdHistory;
	}
	public void setIdHistory(String idHistory) {
		IdHistory = idHistory;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getCodeBank() {
		return CodeBank;
	}
	public void setCodeBank(String codeBank) {
		CodeBank = codeBank;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getAddressDetail() {
		return AddressDetail;
	}
	public void setAddressDetail(String addressDetail) {
		AddressDetail = addressDetail;
	}
	public String getLatitude() {
		return Latitude;
	}
	public void setLatitude(String latitude) {
		Latitude = latitude;
	}
	public String getLongitude() {
		return Longitude;
	}
	public void setLongitude(String longitude) {
		Longitude = longitude;
	}
	public String getAccount() {
		return Account;
	}
	public void setAccount(String account) {
		Account = account;
	}
	public String getNominal() {
		return Nominal;
	}
	public void setNominal(String nominal) {
		Nominal = nominal;
	}
	public String getKeterangan() {
		return Keterangan;
	}
	public void setKeterangan(String keterangan) {
		Keterangan = keterangan;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	
	
	
	
	
}
